
/* Class #61 */

// API
// DOM Document Object Model
// BOM Browser Object Model
// CSSOM Css object Model

/*console.log(window);
console.log(document);
let texto = "Hola";
 const hablar = (texto) => {
    speechSynthesis.speak(new SpeechSynthesisUtterance(texto));
 }
 hablar(texto);*/
console.log("elemenntos del documento");
console.log(document);
console.log(document.head);
console.log(document.body);
console.log(document.doctype);
console.log(document.documentElement);
console.log(document.charset);
console.log(document.title);
console.log(document.link);
console.log(document.images);
console.log(document.forms);
console.log(document.styleSheets);
console.log(document.scripts);
setTimeout(() =>{
    console.log(document.getSelection().toString());
}, 3000);

document.write('hOLAAAAA');

/* Class #62 */

/*console.log(document.getElementsByTagName("li"));
console.log(document.getElementsByClassName("card"));
console.log(document.getElementsByName("nombre"));
console.log(document.getElementById('test'));
console.log('--------');
let te123 = document.getElementById('test');
console.log(te123);

console.log(document.querySelector("#test"));
console.log(document.querySelectorAll("a"));
console.log(document.querySelectorAll("a").length);
document.querySelectorAll("a").forEach((e1)=> console.log(e1));

console.log(document.querySelector(".card"));
console.log(document.querySelectorAll(".card")[3]);
console.log(document.querySelectorAll("#menu li"));
console.log(document.querySelector("#menu li"));
*/

/* Class #63 */

// Atributes
/*
console.log(document.documentElement.lang)
console.log(document.documentElement.getAttribute("lang"))
console.log(document.documentElement.querySelector(".link-dom").href)
console.log(document.documentElement.querySelector(".link-dom").getAttribute('href'))
document.documentElement.lang = "en";

document.documentElement.setAttribute("lang", "es-MX");
console.log(document.documentElement.lang)

// $linkDom, referente a elementos del DOM
const linkDom  = document.querySelector(".link-dom");
linkDom.setAttribute("target", "_blank");
linkDom.setAttribute("rel", "noopener");
linkDom.setAttribute("href", "https://www.google.com");
console.log(linkDom.hasAttribute("rel"));
linkDom.removeAttribute("rel");
console.log(linkDom.hasAttribute("rel"));

// data attribute
console.log(linkDom.getAttribute("data-description"));
console.log(linkDom.dataset);
console.log(linkDom.dataset.description);
console.log(linkDom.dataset.id);

linkDom.setAttribute("data-description", "Modelo de Objeto del documento");
console.log(linkDom.dataset.description);

linkDom.dataset.description = "Suscribite al canal"
console.log(linkDom.dataset.description);
*/

/* class #64 */

// Style
/*
const linkDom  = document.querySelector(".link-dom");
console.log(linkDom.style);
console.log(linkDom.getAttribute("style"));
console.log(linkDom.style.backgroundColor);
console.log(linkDom.style.color);
console.log(window.getComputedStyle(linkDom));
console.log(window.getComputedStyle(linkDom).getPropertyValue("color"));

linkDom.style.setProperty("text-decoration", "none");
linkDom.style.setProperty("display", "block");

linkDom.style.width = "50%";
linkDom.style.textAlign = "center";
linkDom.style.marginLeft = "auto";
linkDom.style.marginRight = "auto";
linkDom.style.padding = "1rem";
linkDom.style.borderRadius = ".5rem";
console.log(linkDom.style);
console.log(window.getComputedStyle(linkDom));

// Variables CSS - custom proporties
const $html = document.documentElement, $body = document.body;

let varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color"),
varYellowColor = getComputedStyle($html).getPropertyValue("--yellow-color");


console.log(varDarkColor, varYellowColor);

$body.style.backgroundColor = varDarkColor;
$body.style.color = varYellowColor;

$html.style.setProperty("--dark-color","#000");
varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color");
console.log(varDarkColor);

//$body.style.setProperty("background-color", varDarkColor);
$body.style.backgroundColor = varDarkColor;
*/

/* Class #65 */

// Classes CSS
/*
const $card = document.querySelector(".card");
console.log($card);
console.log($card.className);
console.log($card.classList);
console.log($card.classList.contains("rotate-45"));
$card.classList.add("rotate-45");
console.log($card.className);
console.log($card.classList);
$card.classList.remove("rotate-45");
console.log($card.classList.contains("rotate-45"));
$card.classList.toggle("rotate-45");
$card.classList.toggle("rotate-45");
$card.classList.toggle("rotate-45");
$card.classList.replace("rotate-45", "rotate-135");
$card.classList.add("opacity-80", "cepia");
$card.classList.toggle("opacity-80", "cepia");
*/

/* Class #66 */

// Classes Text y HTML
/*
const $whatDOM = document.getElementById("que-es");

console.log($whatDOM);

let text = `
    <p>
      El Modelo de Objetos del Documento (<b><i>DOM - Document Object Model</i></b>) es un API para documentos HTML y XML.
    </p>
    <p>
      Éste proveé una representación estructural del documento, permitiendo modificar su contenido y presentación visual mediante código JS.
    </p>
    <p>
      <mark>El DOM no es parte de la especificación de JavaScript, es una API para los navegadores.</mark>
    </p>
  `;

//$whatDOM.innerText = text;
$whatDOM.textContent = text;
$whatDOM.innerHTML = text;
$whatDOM.outerHTML = text;
*/

/* Class #67 */

/**
 * DOM Traversing
 * recoriendo el DOM
 * children: retorna los hijos del elemento
 * parentElement: retorna el padre del documento
 * parentNode: retorna el padre del documento
 * firstChild: retorna el primer hijo del nodo / ese nodo puede ser espacios en blanco o enters
 * firstElementChild: retorna el primer elemento html del nodo
 * lastChild: retorna el ultimo hijo del nodo / ese nodo puede ser espacios en blanco o enters
 * lastElementChild: retorna el ultimo elemento html del nodo
 * previousSibling: retorna el elemento hermano previo al nodo
 * previousElementSibling: retorna el elemento hermano html previo al nodo
 * nextSibling: retorna el elemento hermano posterior al nodo
 * nextElementSibling: retorna el elemento hermano html posterior al nodo
 * closest(method): buscar el ancestro mas cercano de un elemento
 */
/*
const $cards = document.querySelector(".cards");

console.log($cards.children);
console.log($cards.children[2]);
console.log($cards.parentElement);
console.log($cards.parentNode);

console.log($cards.firstChild);
console.log($cards.firstElementChild);
console.log($cards.lastChild);
console.log($cards.lastElementChild);
console.log($cards.previousSibling);
console.log($cards.previousElementSibling);

console.log($cards.nextSibling);
console.log($cards.nextElementSibling);


console.log($cards.closest("div"));
console.log($cards.closest("body"));
console.log($cards.children[2].closest("section"));

*/
/* Class #68 */
/**
 * Creando elementos y fragmentos
 * Crear etiquetas dinamicas
 * createElement(method): crea un elemento html
 * createTextNode(method): crear un nodo de texto
 * querySelector(method): selecionar un elemento
 * appendChild(method): agregar un hijo al elemento
 * setAttribute(method): asigno un valor al atributo del elemento
 * add(method): añade una clase al elemento
 * classList: lista de las clases del elemento
 * textContent: asignar texto al nodo
 * createDocumentFragment(method): crea fragmentos para el nodo
 */
/*
const $figure = document.createElement("figure"), $img = document.createElement("img"),
  $figcaption = document.createElement("figcaption"), $figcaptionText = document.createTextNode("Animals"),
  $cards2 = document.querySelector(".cards");

$figure2 = document.createElement("figure");

$img.setAttribute("src", "https://placeimg.com/200/200/animals");
$img.setAttribute("alt", "Animals");
$figure.classList.add("card");

$figcaption.appendChild($figcaptionText);
$figure.appendChild($img);
$figure.appendChild($figcaption);

$cards2.appendChild($figure);

$figure2.innerHTML = `
<img src="https://placeimg.com/200/200/people" alt="Any">
<figcaption>Peopple</figcaption>
`;
$figure2.classList.add("card");

$cards2.appendChild($figure2);

const estaciones = ["Primavera", "Verano", "Otoño", "Invierno"],
  $ul = document.createElement("ul");

document.write("<h3>Estaciones</h3>");
document.body.appendChild($ul);

estaciones.forEach(el => {
  const $li = document.createElement("li");
  $li.textContent = el;
  $ul.appendChild($li);
});

const continentes = ["África", "América", "Asia", "Europa", "Oceanía"],
  $ul2 = document.createElement("ul");

document.write("<h3>Continentes</h3>");
document.body.appendChild($ul2);
$ul2.innerHTML = "";
continentes.forEach(el => $ul2.innerHTML += `<li>${el}</li>`);

const meses = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
],
  $ul3 = document.createElement("ul"),
  $fragmentos = document.createDocumentFragment();

meses.forEach(el => {
  const $li = document.createElement("li");
  $li.textContent = el;
  $fragmentos.appendChild($li);
});

document.write("<h3>Meses</h3>");
$ul3.appendChild($fragmentos);
document.body.appendChild($ul3);
*/
/* Class #69 */
/**
 * Templates
 * html <template/>: use to hold some content that will be hidden when the page loads
 * createDocumentFragment: create a new empty document
 * importNode: method creates a copy of a Node or documentFragment
 */
/*
const $cards3 = document.querySelector(".cards"),
  $template = document.getElementById("template-card").content,
  $fragment = document.createDocumentFragment(),
  cardsContent = [
    {
      title: "Tecnología",
      img: "https://placeimg.com/200/200/tech",
    },
    {
      title: "Animales",
      img: "https://placeimg.com/200/200/animals",
    },
    {
      title: "Arquitectura",
      img: "https://placeimg.com/200/200/arch",
    },
    {
      title: "Gente",
      img: "https://placeimg.com/200/200/people",
    },
    {
      title: "Naturaleza",
      img: "https://placeimg.com/200/200/nature",
    },
  ];

cardsContent.forEach(el => {
  $template.querySelector("img").setAttribute("src", el.img);
  $template.querySelector("img").setAttribute("alt", el.title);
  $template.querySelector("figcaption").textContent = el.title;

  let $clone = document.importNode($template, true);
  $fragment.appendChild($clone);
})

$cards3.appendChild($fragment);
*/
/* Class #70 */

/**
 * Modificando elementos (old-style)
 * Element.classList: collection of the class attributes of the element
 * Document.createElement(): method creates the HTML element specified by tagName
 * Node.cloneNode(): method of the Node interface returns a duplicate of the node on which this method was called/ true clone all element, false only the children
 * innerHTML: gets or sets the HTML or XML markup contained within the element
 * insertBefore: insert node before the node
 * lastElementChild: last element of the node
 * firstElementChild: first element of the node; 
 */
/*
const $cards4 = document.querySelector(".cards"),
  $newCard = document.createElement("figure"),
  $cloneCards = $cards4.cloneNode(true);
$newCard.innerHTML = `
 <img src="https://placeimg.com/200/200/any" alt="Any">
 <figcaption>Any</figcaption>
`;
$newCard.classList.add("card");
$cards4.replaceChild($newCard, $cards4.children[2]);
$cards4.removeChild($cards4.lastElementChild);
$cards4.insertBefore($newCard, $cards4.firstElementChild);
document.body.appendChild($cloneCards);
*/


/**
 * Class #71
 * insertAdjacent...
 * insertAdjacentElement()
 * insertAdjacentHTML()
 * insertAdjacentText
 * 
 * Posiciones
 * beforebegin(previous sibling)
 * afterbegin(first child)
 * beforeend(last child)
 * afterend(next sibling)
 */

const $cards5 = document.querySelector(".cards"),
  $newCard2 = document.createElement("figure");

let $content = `
<img src="https://placeimg.com/200/200/any" alt="Any">
<figcaption></figcaption>
`;

$newCard2.classList.add("card");
$newCard2.insertAdjacentHTML("beforeend", $content);
$newCard2.querySelector("figcaption").insertAdjacentText("afterbegin", "Any");
//$cards5.insertAdjacentElement("beforebegin", $newCard2);
// $cards5.insertAdjacentElement("afterbegin", $newCard2);
// $cards5.insertAdjacentElement("beforeend", $newCard2);
// $cards5.insertAdjacentElement("afterend", $newCard2);

//$cards5.prepend($newCard2);
//$cards5.before($newCard2);
//$cards5.append($newCard2);
$cards5.after($newCard2);


/**
 * Clase 72
 * event type
 * event target
 */

function holaMundo() {
  alert("hola mundo");
  console.log(event);
}

window.onload = function(){ 
  const $eventoSemnatico = document.getElementById("evento-semnatico");
  //const $eventoMultiple = document.getElementById("evento-multiple");
  $eventoSemnatico.onclick = holaMundo;
  $eventoSemnatico.onclick = function(e){
    alert("manejador de event");
    console.log(e);
  };

  //$eventoMultiple.addEventListener("click", ()=>{console.log('test')});
  const btn = document.getElementById('evento-multiple');
  console.log(btn); // 👉️ null

  // ✅ Check if btn exists before addEventListener()
  if (btn) {
    btn.addEventListener('click', holaMundo);
    btn.addEventListener('click', (e) => {
      console.log('btn clicked');
      console.log(e);
      console.log(e.type);
      console.log(e.target);
      console.log(event);
    });
  }

  // ✅ Using optional chaining (?.)
  btn?.addEventListener('click', () => {
    console.log('btn clicked2');
  });
};

/**
 * Clase 73
 */


